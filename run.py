from pathlib import Path

import streamlit as st
from powershovel.pages.distributions import distributions
from powershovel.pages.runs import runs
from powershovel.pages.viewer import viewer

# from powershovel.modules.helper_functions import open_pickle_file


def _max_width_():
    max_width_str = f"max-width: 2000px;"
    st.markdown(
        f"""
    <style>
    .reportview-container .main .block-container{{
        {max_width_str}
    }}
    </style>
    """,
        unsafe_allow_html=True,
    )


_max_width_()

page = st.sidebar.selectbox("Page", ["Viewer", "Distributions", "Runs"], index=0)

if page == "Viewer":
    viewer()
elif page == "Distributions":
    distributions()
elif page == "Runs":
    runs()
