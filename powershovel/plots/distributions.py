import plotly.graph_objects as go
import powershovel.plots.axis_labels as axis_labels
from plotly.subplots import make_subplots
from powershovel.modules.helper_functions import (convert_data_for_step,
                                                  norm_bar_chart,
                                                  norm_histogram)

template = go.layout.Template()
template.layout["colorway"] = ["#50514F", "#F25F5C", "#FFE066", "#247BA0", "#70C1B3"]
template.layout["font"] = dict(
    family="IBM Plex Mono, monospace", size=12, color="Black"
)


def no_of_events_textbox(fig, data):
    strings = []
    for set_name, values in data.items():
        strings.append("{} events: {}".format(set_name, values["events"]))
    string = "<br>".join(strings)
    fig.update_layout(
        annotations=[
            go.layout.Annotation(
                text=string,
                align="left",
                showarrow=False,
                xref="paper",
                yref="paper",
                x=0.01,
                y=1.2,
                bordercolor="black",
                bgcolor="white",
                borderwidth=1,
            )
        ]
    )


def histogram_1d(data: dict, column: str, density: bool = False):
    xlabel = axis_labels.get_column_label(column)
    ylabel = "Density" if density else "Count"
    title = axis_labels.get_column_title(column)
    fig = make_subplots(rows=1, cols=1)
    for key, value in data.items():
        if density:
            value["hist"] = norm_histogram(value)
        bin_edges, hist = convert_data_for_step(value["bin_edges"], value["hist"])
        fig.add_trace(
            go.Scatter(
                x=bin_edges,
                y=hist,
                name=key + ": " + str(value["events"]),
                mode="lines",
                line={"shape": "vh"},
                showlegend=True,
            ),
            row=1,
            col=1,
        )
        if column == "SplitInIcePulses" or column == "SRTInIcePulses":
            fig.update_layout(yaxis_type="log")
    else:
        fig.update_yaxes(rangemode="tozero")
    fig.update_layout(
        template=template,
        xaxis_title=xlabel,
        yaxis_title=ylabel,
        # xaxis=dict(rangeslider=dict(visible=True)),
        title=title,
        legend=dict(orientation="h", yanchor="bottom", y=1.02, xanchor="right", x=1),
    )
    return fig


def bar_chart(data, column, density):
    xlabel = axis_labels.get_column_label(column)
    ylabel = "Density" if density else "Count"
    title = axis_labels.get_column_title(column)
    fig = make_subplots(rows=1, cols=1)
    for key, value in data.items():
        if density:
            value["hist"] = norm_bar_chart(value)
        fig.add_trace(
            go.Bar(
                x=value["bin_edges"],
                y=value["hist"],
                name=key + ": " + str(value["events"]),
                showlegend=True,
            ),
            row=1,
            col=1,
        )
    fig.update_layout(
        template=template,
        xaxis_title=xlabel,
        yaxis_title=ylabel,
        xaxis=dict(
            tickmode="array",
            tickvals=value["bin_edges"],
        ),
        title=title,
        legend=dict(orientation="h", yanchor="bottom", y=1.02, xanchor="right", x=1),
        barmode="group",
    )
    return fig
