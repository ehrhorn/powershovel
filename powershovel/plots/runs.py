import numpy as np
import plotly.graph_objects as go
import powershovel.plots.axis_labels as axis_labels
from plotly.subplots import make_subplots

template = go.layout.Template()
template.layout["colorway"] = ["#50514F", "#F25F5C", "#FFE066", "#247BA0", "#70C1B3"]
template.layout["font"] = dict(
    family="IBM Plex Mono, monospace", size=12, color="Black"
)


def permutation_importance_bar_chart(histograms_dict, selected_type, selected_bin):
    fig = make_subplots(
        rows=1,
        cols=2,
        subplot_titles=(
            f"{list(histograms_dict.keys())[0]} permutation importance",
            f"{list(histograms_dict.keys())[1]} permutation importance",
        ),
    )
    for i, (key, data) in enumerate(histograms_dict.items()):
        fig.add_trace(
            go.Bar(x=data[1], y=data[0], orientation="h", name=key, showlegend=False),
            row=1,
            col=i + 1,
        )
    fig.update_layout(template=template)
    return fig


def prediction_1d_histogram(data: dict, metric: str, ibin: int = 0):
    xlabel = axis_labels.prediction_1d_histogram_x_axis(metric)
    fig = make_subplots(
        rows=1,
        cols=2,
        subplot_titles=(
            f"{list(data.keys())[0]} {metric} distribution",
            f"{list(data.keys())[1]} {metric} distribution",
        ),
    )
    for i, (key, value) in enumerate(data.items()):
        for j, (subkey, subvalue) in enumerate(value.items()):
            bin_edges = subvalue["bin_edges"]
            hist = subvalue["hist"]
            bin_mids = (bin_edges[:-1] + bin_edges[1:]) / 2
            fig.add_trace(
                go.Scatter(
                    x=bin_mids,
                    y=hist,
                    line=dict(
                        color="Red" if j == 0 else "Blue", shape="hvh", width=0.5
                    ),
                    fill="tozeroy",
                    name="Reconstruction" if j == 0 else "Truth",
                    showlegend=True if i == 0 else False,
                ),
                row=1,
                col=i + 1,
            )
    fig["layout"]["yaxis1"].update(title="Density")
    fig["layout"]["yaxis2"].update(title="Density")
    fig["layout"]["xaxis1"].update(title=xlabel)
    fig["layout"]["xaxis2"].update(title=xlabel)
    fig.update_layout(template=template)
    return fig


def resolution_1d_histogram(data: dict, metric: str, ibin: int = 0):
    max_bar = []
    min_bar = []
    max_res = []
    min_res = []
    min_x = []
    max_x = []
    max_y = []
    fig = make_subplots(
        rows=2,
        cols=2,
        subplot_titles=(
            f"{list(data.keys())[0]}, {data[list(data.keys())[0]]['events']} events",
            f"{list(data.keys())[1]}, {data[list(data.keys())[1]]['events']} events",
        ),
        specs=[
            [{"secondary_y": True}, {"secondary_y": True}],
            [{"secondary_y": False}, {"secondary_y": False}],
        ],
        vertical_spacing=0.1,
        # horizontal_spacing=0.1,
    )
    performance = []
    for i, (key, value) in enumerate(data.items()):
        colors = ["black"] * len(value["mid_percentiles"])
        colors[ibin] = "red"
        bin_edges = value["bin_edges"]
        hist = value["hist"]
        resolution = value["resolution"]
        bin_mids = (bin_edges[:-1] + bin_edges[1:]) / 2
        performance.append((bin_mids, resolution))
        max_bar.append(max(hist))
        min_bar.append(min(hist))
        max_res.append(max(resolution))
        min_res.append(min(resolution))
        fig.add_trace(
            go.Scatter(
                x=bin_mids,
                y=resolution,
                name=key,
                showlegend=False,
                xaxis="x1" if i == 0 else "x2",
                mode="markers",
                marker=dict(color=colors),
                error_y=dict(
                    type="data",
                    symmetric=False,
                    array=value["resolution_sigma_high"],
                    arrayminus=value["resolution_sigma_low"],
                ),
            ),
            secondary_y=False,
            row=1,
            col=i + 1,
        )
        fig.add_trace(
            go.Bar(
                x=bin_mids,
                y=hist,
                name=key,
                showlegend=False,
                xaxis="x1" if i == 0 else "x2",
                yaxis="y3" if i == 0 else "y4",
                marker=dict(color="grey", opacity=0.5),
            ),
            secondary_y=True,
            row=1,
            col=i + 1,
        )
        for j in range(len(value["percentile_histograms"])):
            min_x.append(np.amin(value["percentile_histograms"][j]["bin_edges"]))
            max_x.append(np.amax(value["percentile_histograms"][j]["bin_edges"]))
            max_y.append(np.amax(value["percentile_histograms"][j]["hist"]))
        bin_edges = value["percentile_histograms"][ibin]["bin_edges"]
        hist = value["percentile_histograms"][ibin]["hist"]
        bin_mids = (bin_edges[:-1] + bin_edges[1:]) / 2
        # fig["layout"]["annotations"] += (
        #     go.layout.Annotation(
        #         x=0.02 if i == 0 else 0.98,
        #         y=0.99,
        #         showarrow=False,
        #         text="Events: {:,}".format(value["events"]),
        #         xref="paper",
        #         yref="paper",
        #         bgcolor="white",
        #         bordercolor="#c7c7c7",
        #         borderwidth=2,
        #         borderpad=4,
        #         align="left" if i == 0 else "right",
        #     ),
        # )
    perf0 = (performance[1][1] - performance[0][1]) / performance[0][1]
    perf1 = (performance[0][1] - performance[1][1]) / performance[1][1]
    for i, run in enumerate([perf0, perf1]):
        fig.add_trace(
            go.Scatter(
                x=performance[i][0],
                y=run,
                showlegend=False,
                xaxis="x1" if i == 0 else "x2",
                yaxis="y5" if i == 0 else "y6",
                mode="markers",
                marker=dict(color="red", opacity=1),
            ),
            secondary_y=False,
            row=2,
            col=i + 1,
        )
    max_bar = max(max_bar)
    min_bar = min(min_bar)
    max_bar = np.floor(np.log10(max_bar)) + 1
    min_bar = np.floor(np.log10(min_bar))
    max_res = max(max_res) * 1.10
    min_res = min(min_res) * 0.90
    min_x = min(min_x)
    max_x = max(max_x)
    max_y = max(max_y)
    fig["layout"]["xaxis3"].update(title="log(E) [E/ GeV]")
    fig["layout"]["xaxis4"].update(title="log(E) [E/ GeV]")
    fig["layout"]["yaxis1"].update(
        title="Resolution", range=[0, max_res], domain=[0.3, 1.0]
    )
    fig["layout"]["yaxis2"].update(title="Events", type="log", range=[min_bar, max_bar])
    fig["layout"]["yaxis3"].update(
        title="Resolution", range=[0, max_res], domain=[0.3, 1.0]
    )
    fig["layout"]["yaxis4"].update(title="Events", type="log", range=[min_bar, max_bar])
    fig["layout"]["yaxis5"].update(domain=[0.0, 0.2], title="Rel. imp.")
    fig["layout"]["yaxis6"].update(domain=[0.0, 0.2], title="Rel. imp.")
    fig.update_layout(
        template=template,
        # xaxis_title=xlabel,
        # yaxis_title=ylabel,
        # title=title,
        legend=dict(orientation="h", yanchor="bottom", y=1.02, xanchor="right", x=1),
        height=500,
    )
    return fig


def error_2d_histogram(data: dict, metric: str, ibin: int = 0):
    xlabel = axis_labels.prediction_2d_histogram_x_axis(metric)
    ylabel = axis_labels.prediction_2d_histogram_y_axis(metric)
    title = metric + " errors"
    min_x = []
    max_x = []
    max_y = []
    max_z = []
    bin_values = {}
    for i, (key, value) in enumerate(data.items()):
        bin_values[i] = (
            "["
            + str(round(value["percentile_histograms"][ibin]["resolution_bin"][0], 2))
            + ", "
            + str(round(value["percentile_histograms"][ibin]["resolution_bin"][1], 2))
            + ")"
        )
    fig = make_subplots(
        rows=2,
        cols=2,
        shared_xaxes=False,
        shared_yaxes=False,
        subplot_titles=(
            list(data.keys())[0],
            list(data.keys())[1],
            "Errors in bin {}".format(bin_values[0]),
            "Errors in bin {}".format(bin_values[1]),
        ),
        vertical_spacing=0.1,
    )
    for i, (key, value) in enumerate(data.items()):
        x_line = np.linspace(
            min(value["bin_edges"][1]), max(value["bin_edges"][1]), 100
        )
        y_line = [0] * len(x_line)
        max_z.append(np.amax(value["hist"]))
        value["hist"][value["hist"] == 0] = np.nan
        fig.add_trace(
            go.Heatmap(
                z=value["hist"],
                x=value["bin_edges"][1],
                y=value["bin_edges"][0],
                name=key,
                showlegend=False,
                connectgaps=False,
                showscale=True if i == 0 else False,
                xaxis="x1" if i == 0 else "x2",
                yaxis="y1" if i == 0 else "y2",
                coloraxis="coloraxis",
            ),
            row=1,
            col=i + 1,
        )
        fig.add_trace(
            go.Scatter(
                x=x_line,
                y=y_line,
                line=dict(dash="dot", color="red"),
                showlegend=False,
            ),
            row=1,
            col=i + 1,
        )
        colors = ["black"] * len(value["mid_percentiles"])
        colors[ibin] = "red"
        fig.add_trace(
            go.Scatter(
                x=value["percentile_bin_mids"],
                y=value["mid_percentiles"],
                showlegend=False,
                mode="markers",
                marker=dict(color=colors),
                error_y=dict(
                    type="data",
                    symmetric=False,
                    array=value["upper_percentiles"] - value["mid_percentiles"],
                    arrayminus=value["mid_percentiles"] - value["lower_percentiles"],
                ),
            ),
            row=1,
            col=i + 1,
        )
        for j in range(len(value["percentile_histograms"])):
            min_x.append(np.amin(value["percentile_histograms"][j]["bin_edges"]))
            max_x.append(np.amax(value["percentile_histograms"][j]["bin_edges"]))
            max_y.append(np.amax(value["percentile_histograms"][j]["hist"]))
        bin_edges = value["percentile_histograms"][ibin]["bin_edges"]
        hist = value["percentile_histograms"][ibin]["hist"]
        bin_mids = (bin_edges[:-1] + bin_edges[1:]) / 2
        fig.add_trace(
            go.Scatter(
                x=bin_mids,
                y=hist,
                line=dict(color="Red", shape="hvh", width=0.5),
                fill="tozeroy",
                name=key,
                showlegend=False,
                xaxis="x3" if i == 0 else "x4",
                yaxis="y3" if i == 0 else "y4",
            ),
            row=2,
            col=i + 1,
        )
        fig.add_shape(
            dict(
                type="line",
                xref="x3" if i == 0 else "x4",
                yref="paper",
                x0=value["lower_percentiles"][ibin],
                y0=0,
                x1=value["lower_percentiles"][ibin],
                y1=1,
                line=dict(color="RoyalBlue", width=1),
            ),
            row=2,
            col=i + 1,
        )
        fig.add_shape(
            dict(
                type="line",
                xref="x3" if i == 0 else "x4",
                yref="paper",
                x0=value["mid_percentiles"][ibin],
                y0=0,
                x1=value["mid_percentiles"][ibin],
                y1=1,
                line=dict(color="RoyalBlue", width=1),
            ),
            row=2,
            col=i + 1,
        )
        fig.add_shape(
            dict(
                type="line",
                xref="x3" if i == 0 else "x4",
                yref="paper",
                x0=value["upper_percentiles"][ibin],
                y0=0,
                x1=value["upper_percentiles"][ibin],
                y1=1,
                line=dict(color="RoyalBlue", width=1),
            ),
            row=2,
            col=i + 1,
        )
        fig["layout"]["annotations"] += (
            go.layout.Annotation(
                x=0.02 if i == 0 else 0.98,
                y=0.99,
                showarrow=False,
                text="Events: {:,}".format(value["events"]),
                xref="paper",
                yref="paper",
                bgcolor="white",
                bordercolor="#c7c7c7",
                borderwidth=2,
                borderpad=4,
                align="left" if i == 0 else "right",
            ),
        )
        fig["layout"]["annotations"] += (
            go.layout.Annotation(
                x=0.02 if i == 0 else 0.98,
                y=0.43,
                showarrow=False,
                text="Events: {:,}".format(
                    value["percentile_histograms"][ibin]["events"]
                ),
                xref="paper",
                yref="paper",
                bgcolor="white",
                bordercolor="#c7c7c7",
                borderwidth=2,
                borderpad=4,
                align="left" if i == 0 else "right",
            ),
        )
    min_x = min(min_x)
    max_x = max(max_x)
    max_y = max(max_y)
    max_z = max(max_z)
    fig.update_traces(zmin=0, zmax=max_z, selector=dict(type="heatmap"))
    fig.update_shapes(dict(y1=max_y))
    fig.update_layout(
        template=template,
        xaxis_title=xlabel,
        yaxis_title=ylabel,
        title=title,
        legend=dict(orientation="h", yanchor="bottom", y=1.02, xanchor="right", x=1),
        height=1200,
    )
    fig["layout"]["coloraxis"].update(
        colorscale="Oranges",
        colorbar=dict(
            title="Density", y=0.83, lenmode="fraction", len=0.4, thickness=20
        ),
    )
    fig["layout"]["yaxis1"].update(dict(scaleanchor="x1", scaleratio=1))
    fig["layout"]["yaxis2"].update(dict(scaleanchor="x2", scaleratio=1))
    fig["layout"]["xaxis2"].update(matches="x1", title=xlabel)
    fig["layout"]["yaxis2"].update(matches="y1", title=xlabel)
    fig["layout"]["xaxis3"].update(range=[min_x, max_x], title=xlabel)
    fig["layout"]["xaxis4"].update(matches="x3", range=[min_x, max_x], title=xlabel)
    fig["layout"]["yaxis3"].update(title="Density")
    fig["layout"]["yaxis4"].update(matches="y3", title="Density")
    return fig


def prediction_2d_histogram(data: dict, metric: str, ibin: int = 0):
    xlabel = axis_labels.prediction_2d_histogram_x_axis(metric)
    ylabel = axis_labels.prediction_2d_histogram_y_axis(metric)
    title = metric + " predictions"
    min_x = []
    max_x = []
    max_y = []
    max_z = []
    bin_values = {}
    for i, (key, value) in enumerate(data.items()):
        bin_values[i] = (
            "["
            + str(round(value["percentile_histograms"][ibin]["resolution_bin"][0], 2))
            + ", "
            + str(round(value["percentile_histograms"][ibin]["resolution_bin"][1], 2))
            + ")"
        )
    fig = make_subplots(
        rows=2,
        cols=2,
        shared_xaxes=False,
        shared_yaxes=False,
        subplot_titles=(
            list(data.keys())[0],
            list(data.keys())[1],
            "Predictions in bin {}".format(bin_values[0]),
            "Predictions in bin {}".format(bin_values[1]),
        ),
        vertical_spacing=0.1,
    )
    for i, (key, value) in enumerate(data.items()):
        x_line = np.linspace(
            min(value["bin_edges"][0]), max(value["bin_edges"][0]), 100
        )
        y_line = np.linspace(
            min(value["bin_edges"][0]), max(value["bin_edges"][0]), 100
        )
        max_z.append(np.amax(value["hist"]))
        value["hist"][value["hist"] == 0] = np.nan
        fig.add_trace(
            go.Heatmap(
                z=value["hist"],
                x=value["bin_edges"][0],
                y=value["bin_edges"][1],
                name=key,
                showlegend=False,
                connectgaps=False,
                showscale=True if i == 0 else False,
                xaxis="x1" if i == 0 else "x2",
                yaxis="y1" if i == 0 else "y2",
                coloraxis="coloraxis",
            ),
            row=1,
            col=i + 1,
        )
        fig.add_trace(
            go.Scatter(
                x=x_line, y=y_line, line=dict(dash="dot", color="red"), showlegend=False
            ),
            row=1,
            col=i + 1,
        )
        colors = ["black"] * len(value["mid_percentiles"])
        colors[ibin] = "red"
        fig.add_trace(
            go.Scatter(
                x=value["mid_percentiles"],
                y=value["percentile_bin_mids"],
                showlegend=False,
                mode="markers",
                marker=dict(color=colors),
                error_x=dict(
                    type="data",
                    symmetric=False,
                    array=value["upper_percentiles"] - value["mid_percentiles"],
                    arrayminus=value["mid_percentiles"] - value["lower_percentiles"],
                ),
            ),
            row=1,
            col=i + 1,
        )
        for j in range(len(value["percentile_histograms"])):
            min_x.append(np.amin(value["percentile_histograms"][j]["bin_edges"]))
            max_x.append(np.amax(value["percentile_histograms"][j]["bin_edges"]))
            max_y.append(np.amax(value["percentile_histograms"][j]["hist"]))
        bin_edges = value["percentile_histograms"][ibin]["bin_edges"]
        hist = value["percentile_histograms"][ibin]["hist"]
        bin_mids = (bin_edges[:-1] + bin_edges[1:]) / 2
        min_x.append(np.amin(bin_edges))
        max_x.append(np.amax(bin_edges))
        fig.add_trace(
            go.Scatter(
                x=bin_mids,
                y=hist,
                line=dict(color="Red", shape="hvh", width=0.5),
                fill="tozeroy",
                name=key,
                showlegend=False,
                xaxis="x3" if i == 0 else "x4",
                yaxis="y3" if i == 0 else "y4",
            ),
            row=2,
            col=i + 1,
        )
        fig.add_shape(
            dict(
                type="line",
                xref="x3" if i == 0 else "x4",
                yref="paper",
                x0=value["lower_percentiles"][ibin],
                y0=0,
                x1=value["lower_percentiles"][ibin],
                y1=1,
                line=dict(color="RoyalBlue", width=1),
            ),
            row=2,
            col=i + 1,
        )
        fig.add_shape(
            dict(
                type="line",
                xref="x3" if i == 0 else "x4",
                yref="paper",
                x0=value["mid_percentiles"][ibin],
                y0=0,
                x1=value["mid_percentiles"][ibin],
                y1=1,
                line=dict(color="RoyalBlue", width=1),
            ),
            row=2,
            col=i + 1,
        )
        fig.add_shape(
            dict(
                type="line",
                xref="x3" if i == 0 else "x4",
                yref="paper",
                x0=value["upper_percentiles"][ibin],
                y0=0,
                x1=value["upper_percentiles"][ibin],
                y1=1,
                line=dict(color="RoyalBlue", width=1),
            ),
            row=2,
            col=i + 1,
        )
        fig.add_shape(
            dict(
                type="rect",
                xref="x3" if i == 0 else "x4",
                yref="paper",
                x0=value["percentile_histograms"][ibin]["resolution_bin"][0],
                y0=0,
                x1=value["percentile_histograms"][ibin]["resolution_bin"][1],
                y1=1,
                opacity=0.5,
                fillcolor="#d3d3d3",
                line_color="#d3d3d3",
            ),
            row=2,
            col=i + 1,
        )
        fig["layout"]["annotations"] += (
            go.layout.Annotation(
                x=0.02 if i == 0 else 0.98,
                y=0.99,
                showarrow=False,
                text="Events: {:,}".format(value["events"]),
                xref="paper",
                yref="paper",
                bgcolor="white",
                bordercolor="#c7c7c7",
                borderwidth=2,
                borderpad=4,
                align="left" if i == 0 else "right",
            ),
        )
        fig["layout"]["annotations"] += (
            go.layout.Annotation(
                x=0.02 if i == 0 else 0.98,
                y=0.43,
                showarrow=False,
                text="Events: {:,}".format(
                    value["percentile_histograms"][ibin]["events"]
                ),
                xref="paper",
                yref="paper",
                bgcolor="white",
                bordercolor="#c7c7c7",
                borderwidth=2,
                borderpad=4,
                align="left" if i == 0 else "right",
            ),
        )
    min_x = min(min_x)
    max_x = max(max_x)
    max_y = max(max_y)
    max_z = max(max_z)
    fig.update_traces(zmin=0, zmax=max_z, selector=dict(type="heatmap"))
    fig.update_shapes(dict(y1=max_y))
    fig.update_layout(
        template=template,
        xaxis_title=xlabel,
        yaxis_title=ylabel,
        title=title,
        legend=dict(orientation="h", yanchor="bottom", y=1.02, xanchor="right", x=1),
        height=1200,
    )
    fig["layout"]["coloraxis"].update(
        colorscale="Viridis",
        colorbar=dict(
            title="Density", y=0.83, lenmode="fraction", len=0.4, thickness=20
        ),
    )
    fig["layout"]["yaxis1"].update(dict(scaleanchor="x1", scaleratio=1))
    fig["layout"]["yaxis2"].update(dict(scaleanchor="x2", scaleratio=1))
    fig["layout"]["xaxis2"].update(matches="x1", title=xlabel)
    fig["layout"]["yaxis2"].update(matches="y1", title=xlabel)
    fig["layout"]["xaxis3"].update(range=[min_x, max_x], title=xlabel)
    fig["layout"]["xaxis4"].update(matches="x3", range=[min_x, max_x], title=xlabel)
    fig["layout"]["yaxis3"].update(title="Density")
    fig["layout"]["yaxis4"].update(matches="y3", range=[0, 3], title="Density")
    return fig
