import numpy as np
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots


def shifted_direction_vectors(direction, position, scale):
    x = [
        position[0, 0] - direction[0, 0] * scale,
        direction[0, 0] * scale + position[0, 0],
    ]
    y = [
        position[0, 1] - direction[0, 1] * scale,
        direction[0, 1] * scale + position[0, 1],
    ]
    z = [
        position[0, 2] - direction[0, 2] * scale,
        direction[0, 2] * scale + position[0, 2],
    ]
    return np.array([x, y, z])


def direction_vectors(direction, scale):
    x = [0, direction[0, 0] * scale]
    y = [0, direction[0, 1] * scale]
    z = [0, direction[0, 2] * scale]
    return np.array([x, y, z])


def normalize_function(x, a=0, b=20):
    normalized = ()


def plotly_event(pulses, geom, truth, reconstruction):
    true_dirs = truth[["direction_x", "direction_y", "direction_z"]].values
    true_pos = truth[["position_x", "position_y", "position_z"]].values
    true_vector = direction_vectors(true_dirs, 100000)
    shifted_true_vector = shifted_direction_vectors(true_dirs, true_pos, 100000)
    if reconstruction is not None:
        if all(
            elem in reconstruction.columns.values.tolist()
            for elem in ["direction_x", "direction_y", "direction_z"]
        ):
            own_dirs = reconstruction[
                ["direction_x", "direction_y", "direction_z"]
            ].values
            own_vector = direction_vectors(own_dirs, 100000)
        elif reconstruction.columns.values.tolist() == ["zenith"]:
            own_z = np.cos(np.deg2rad(reconstruction["zenith"].values[0]))
            truth_z = truth["zenith"].values[0]
    a = 1
    b = 40
    min_charge = pulses["charge_log10"].min()
    max_charge = pulses["charge_log10"].max()
    norm_func = lambda x: ((b - a) * (x - min_charge) / (max_charge - min_charge) + a)
    pulses["charge_log10"] = pulses["charge_log10"].apply(norm_func)
    fig = px.scatter_3d(
        pulses,
        x="dom_x",
        y="dom_y",
        z="dom_z",
        color="time",
        size="charge_log10",
        size_max=40,
        opacity=1.0,
    )
    fig.add_scatter3d(
        x=geom.dom_x.values,
        y=geom.dom_y.values,
        z=geom.dom_z.values,
        mode="markers",
        name="DOMs",
        marker=dict(
            size=1,
            color="black",
        ),
    )
    # fig.add_scatter3d(
    #     x=true_vector[0, :],
    #     y=true_vector[1, :],
    #     z=true_vector[2, :],
    #     mode="lines",
    #     name="truth",
    #     line=dict(color="blue", width=1),
    # )
    fig.add_scatter3d(
        x=shifted_true_vector[0, :],
        y=shifted_true_vector[1, :],
        z=shifted_true_vector[2, :],
        mode="lines",
        name="truth",
        line=dict(color="blue", width=0.1),
    )
    fig.add_scatter3d(
        x=[true_pos[0, 0]],
        y=[true_pos[0, 1]],
        z=[true_pos[0, 2]],
        mode="markers",
        name="true vertex",
        showlegend=False,
        marker=dict(color="blue"),
    )
    if reconstruction is not None:
        if all(
            elem in reconstruction.columns.values.tolist()
            for elem in ["direction_x", "direction_y", "direction_z"]
        ):
            fig.add_scatter3d(
                x=own_vector[0, :],
                y=own_vector[1, :],
                z=own_vector[2, :],
                mode="lines",
                name="reconstruction",
                line=dict(color="red", width=1),
            )
        # elif reconstruction.columns.values.tolist() == ["zenith"]:
        #     fig.add_scatter3d(
        #         x=[0],
        #         y=[0],
        #         z=[own_z],
        #         mode="markers",
        #         name="reconstruction",
        #         marker=dict(color="red"),
        #     )
        #     fig.add_scatter3d(
        #         x=[0],
        #         y=[0],
        #         z=[truth_z],
        #         mode="markers",
        #         name="truth",
        #         marker=dict(color="blue"),
        #     )
    fig.update_layout(
        scene=dict(
            xaxis=dict(range=[-800, 800]),
            yaxis=dict(range=[-800, 800]),
            zaxis=dict(range=[-800, 800]),
        ),
        height=800,
        scene_aspectmode="cube",
        legend=dict(x=-0.1, y=1.2, orientation="h"),
    )
    return fig


# def plotly_event(pulses, geom, truth):
#     # own_dirs = predictions[['direction_x', 'direction_y', 'direction_z']].values
#     # own_pos = predictions[['position_x', 'position_y', 'position_z']].values

#     true_dirs = truth[["direction_x", "direction_y", "direction_z"]].values
#     true_pos = truth[["position_x", "position_y", "position_z"]].values

#     # opp_dirs = comparison[['direction_x', 'direction_y', 'direction_y']].values
#     # opp_pos = comparison[['position_x', 'position_y', 'position_z']].values

#     # own_vector = direction_vectors(own_dirs, own_pos, 100000)
#     true_vector = direction_vectors(true_dirs, true_pos, 100000)
#     # opp_vector = direction_vectors(opp_dirs, opp_pos, 100000)

#     charge = pulses["charge_log10"]
#     a = 1
#     b = 40
#     normed_charges = (
#         (b - a) * (charge - charge.min()) / (charge.max() - charge.min()) + a
#     ).values.tolist()
#     trace1 = go.Scatter3d(
#         x=pulses.dom_x.values,
#         y=pulses.dom_y.values,
#         z=pulses.dom_z.values,
#         mode="markers",
#         name="pulses",
#         marker=dict(
#             color=pulses.time.values,
#             size=normed_charges,
#             colorscale="Viridis",
#             opacity=1.0,
#             colorbar=dict(thickness=20),
#         ),
#     )

#     trace2 = go.Scatter3d(
#         x=geom.dom_x.values,
#         y=geom.dom_y.values,
#         z=geom.dom_z.values,
#         mode="markers",
#         name="DOMs",
#         marker=dict(
#             size=1,
#             color="black",
#             # opacity=0.5
#         ),
#     )

#     # trace3 = go.Scatter3d(
#     #     x=own_vector[0, :],
#     #     y=own_vector[1, :],
#     #     z=own_vector[2, :],
#     #     mode='lines',
#     #     name=selected_run,
#     #     marker=dict(
#     #         color='red',
#     #         size=0.1
#     #     )
#     # )

#     # trace4 = go.Scatter3d(
#     #     x=opp_vector[0, :],
#     #     y=opp_vector[1, :],
#     #     z=opp_vector[2, :],
#     #     mode='lines',
#     #     name=selected_comparison,
#     #     marker=dict(
#     #         color='green',
#     #         size=0.1
#     #     )
#     # )

#     trace5 = go.Scatter3d(
#         x=true_vector[0, :],
#         y=true_vector[1, :],
#         z=true_vector[2, :],
#         # mode="lines",
#         name="truth",
#         line=dict(color="blue", width=0.5),
#     )

#     # trace6 = go.Scatter3d(
#     #     x=[own_pos[0, 0]],
#     #     y=[own_pos[0, 1]],
#     #     z=[own_pos[0, 2]],
#     #     mode='markers',
#     #     name=selected_run + ' vertex',
#     #     showlegend=False,
#     #     marker=dict(
#     #         color='red'
#     #     )
#     # )

#     # trace7 = go.Scatter3d(
#     #     x=[opp_pos[0, 0]],
#     #     y=[opp_pos[0, 1]],
#     #     z=[opp_pos[0, 2]],
#     #     mode='markers',
#     #     name=selected_comparison + ' vertex',
#     #     showlegend=False,
#     #     marker=dict(
#     #         color='green'
#     #     )
#     # )

#     trace8 = go.Scatter3d(
#         x=[true_pos[0, 0]],
#         y=[true_pos[0, 1]],
#         z=[true_pos[0, 2]],
#         mode="markers",
#         name="true vertex",
#         showlegend=False,
#         marker=dict(color="blue"),
#     )

#     # data = [trace1, trace2, trace3, trace4, trace5, trace6, trace7, trace8]
#     data = [trace1, trace2, trace5, trace8]
#     fig = go.Figure(data=data)
#     fig.update_layout(
#         scene=dict(
#             xaxis=dict(range=[-800, 800]),
#             yaxis=dict(range=[-800, 800]),
#             zaxis=dict(range=[-800, 800]),
#         ),
#         height=800,
#         scene_aspectmode="cube",
#         legend=dict(x=-0.1, y=1.2, orientation="h"),
#     )
#     fig.update_layout(margin=dict(l=0, r=0, b=0, t=0))

#     # Add dropdown
#     fig.update_layout(
#         updatemenus=[
#             dict(
#                 type = "buttons",
#                 direction = "left",
#                 buttons=list([
#                     dict(
#                         args=["type", "surface"],
#                         label="3D Surface",
#                         method="restyle"
#                     ),
#                     dict(
#                         args=["type", "heatmap"],
#                         label="Heatmap",
#                         method="restyle"
#                     )
#                 ]),
#                 pad={"r": 10, "t": 10},
#                 showactive=True,
#                 x=0.11,
#                 xanchor="left",
#                 y=1.1,
#                 yanchor="top"
#             ),
#         ]
#     )
#     return fig
