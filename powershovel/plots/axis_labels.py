def get_binning_label(selected_binning):
    if selected_binning == "energy_log10_truth":
        label = "$\\log_{10}(E_{\\text{true}}) \\ \\left(E / \\si{\\giga\\electronvolt} \\right)$"
    elif selected_binning == "azimuth_truth":
        label = "$\\phi_{\\text{truth}} \\ \\left( \\si{\\degree} \\right)$"
    elif selected_binning == "zenith_truth":
        label = "$\\theta_{\\text{truth}} \\ \\left( \\si{\\degree} \\right)$"
    elif selected_binning == "split_in_ice_pulses_event_length":
        label = "Split in ice pulses event length"
    elif selected_binning == "srt_in_ice_pulses_event_length":
        label = "SRT in ice pulses event length"
    elif selected_binning == "time_truth":
        label = "$t_{\\text{true}} \\ \\left( \\si{\\nano\\second} \\right)$"
    else:
        label = "NO LABEL SET"
    return label


def get_prediction_label(metric_name):
    if metric_name == "energy_log10":
        label = "$\\log_{10}(E_{\\text{reco}}) \\ \\left(E / \\si{\\giga\\electronvolt} \\right)$"
    elif metric_name == "azimuth":
        label = "$\\phi_{\\text{reco}} \\ \\left( \\si{\\degree} \\right)$"
    elif metric_name == "zenith":
        label = "$\\theta_{\\text{reco}} \\ \\left( \\si{\\degree} \\right)$"
    else:
        label = "UNKOWN PREDICTION"
    return label


def get_column_label(column: str) -> str:
    if column == "string":
        label = "String no."
    elif column == "dom":
        label = "DOM no."
    elif column == "pmt":
        label = "PMT no."
    elif column == "dom_x":
        label = "DOM x coordinate (m)"
    elif column == "dom_y":
        label = "DOM y coordinate (m)"
    elif column == "dom_z":
        label = "DOM z coordinate (m)"
    elif column == "pmt_x":
        label = "PMT unit vector x coordinate (m)"
    elif column == "pmt_y":
        label = "PMT unit vector y coordinate (m)"
    elif column == "pmt_z":
        label = "PMT unit vector z coordinate (m)"
    elif column == "pmt_area":
        label = "PMT area (m^2)"
    elif column == "time":
        label = "Time (ns)"
    elif column == "charge_log10":
        label = "Charge"
    elif column == "lc":
        label = "DOM local coincidence (no / yes)"
    elif column == "pulse_width":
        label = "DOM pulse width"
    elif column == "SplitInIcePulses":
        label = "Event length (SplitInIcePulses)"
    elif column == "SRTInIcePulses":
        label = "Event length (SRTInIcePulses)"
    elif column == "energy_log10":
        label = "log<sub>10</sub> E (E / GeV)"
    elif column == "position_x":
        label = "Particle vertex x position (m)"
    elif column == "position_y":
        label = "Particle vertex y position (m)"
    elif column == "position_z":
        label = "Particle vertex z position (m)"
    elif column == "direction_x":
        label = "Particle x direction (m)"
    elif column == "direction_y":
        label = "Particle y direction (m)"
    elif column == "direction_z":
        label = "Particle z direction (m)"
    elif column == "azimuth":
        label = "Particle azimuth direction (rad)"
    elif column == "zenith":
        label = "Particle zenith direction (rad)"
    elif column == "pid":
        label = "Particle PDG ID"
    elif column == "interaction_type":
        label = "Interaction type (CC / NC)"
    elif column == "muon_track_length":
        label = "Muon track length (m)"
    elif column == "stopped_muon":
        label = "Stopped muon (yes/no)"
    else:
        label = "UNKOWN PREDICTION"
    return label


def get_column_title(column):
    if column == "string":
        label = "Pulse hit string no."
    elif column == "dom":
        label = "Pulse hit DOM no."
    elif column == "pmt":
        label = "Pulse hit PMT no."
    elif column == "dom_x":
        label = "Pulse hit DOM x coordinate"
    elif column == "dom_y":
        label = "Pulse hit DOM y coordinate"
    elif column == "dom_z":
        label = "Pulse hit DOM z coordinate"
    elif column == "pmt_x":
        label = "Pulse hit PMT unit vector x coordinate"
    elif column == "pmt_y":
        label = "Pulse hit PMT unit vector y coordinate"
    elif column == "pmt_z":
        label = "Pulse hit PMT unit vector z coordinate"
    elif column == "pmt_area":
        label = "Pulse hit PMT area"
    elif column == "dom_time":
        label = "Pulse hit time"
    elif column == "charge_log10":
        label = "Pulse hit charge"
    elif column == "lc":
        label = "Pulse hit DOM local coincidence"
    elif column == "pulse_width":
        label = "Pulse hit DOM pulse width"
    elif column == "SplitInIcePulses":
        label = "Event length (SplitInIcePulses)"
    elif column == "SRTInIcePulses":
        label = "Event length (SRTInIcePulses)"
    elif column == "energy_log10":
        label = "Particle energy"
    elif column == "time":
        label = "Particle interaction time"
    elif column == "position_x":
        label = "Particle vertex x position"
    elif column == "position_y":
        label = "Particle vertex y position"
    elif column == "position_z":
        label = "Particle vertex z position"
    elif column == "direction_x":
        label = "Particle x direction (going to)"
    elif column == "direction_y":
        label = "Particle y direction (going to)"
    elif column == "direction_z":
        label = "Particle z direction (going to)"
    elif column == "azimuth":
        label = "Particle azimuth direction (coming from)"
    elif column == "zenith":
        label = "Particle zenith direction (coming from)"
    elif column == "pid":
        label = "Particle PDG ID"
    elif column == "interaction_type":
        label = "Particle interaction type"
    else:
        label = "UNKOWN PREDICTION"
    return label


def get_metric_label(selected_metric):
    if selected_metric == "energy_log10":
        label = "$\\log_{10}\\left( E_{\\text{reco}} / E_{\\text{true}} \\right) \\ \\left(E / \\si{\\giga\\electronvolt} \\right)$"
    elif selected_metric == "time":
        label = "$t_{\\text{reco}} - t_{\\text{truth}} \\ \\left( \\si{\\nano\\second} \\right)$"
    elif selected_metric == "position_x":
        label = "$\\bm{p}_{x, \\text{reco}} - \\bm{p}_{x, \\text{truth}} \\ \\left( \\si{\\meter} \\right)$"
    elif selected_metric == "position_y":
        label = "$\\bm{p}_{y, \\text{reco}} - \\bm{p}_{y, \\text{truth}} \\ \\left( \\si{\\meter} \\right)$"
    elif selected_metric == "position_z":
        label = "$\\bm{p}_{z, \\text{reco}} - \\bm{p}_{z, \\text{truth}} \\ \\left( \\si{\\meter} \\right)$"
    elif selected_metric == "direction_x":
        label = "$\\bm{r}_{x, \\text{reco}} - \\bm{r}_{x, \\text{truth}} \\ \\left( \\si{\\meter} \\right)$"
    elif selected_metric == "direction_y":
        label = "$\\bm{r}_{y, \\text{reco}} - \\bm{r}_{y, \\text{truth}} \\ \\left( \\si{\\meter} \\right)$"
    elif selected_metric == "direction_z":
        label = "$\\bm{r}_{z, \\text{reco}} - \\bm{r}_{z, \\text{truth}} \\ \\left( \\si{\\meter} \\right)$"
    elif selected_metric == "azimuth":
        label = "$\\phi_{\\text{reco}} - \\phi_{\\text{truth}} \\ \\left( \\si{\\degree} \\right)$"
    elif selected_metric == "zenith":
        label = "$\\theta_{\\text{reco}} - \\theta_{\\text{truth}} \\ \\left( \\si{\\degree} \\right)$"
    else:
        label = "NO LABEL SET"
    return label


def prediction_2d_histogram_x_axis(name):
    if name == "Energy":
        label = "log<sub>10</sub> E<sub>prediction</sub> (E / GeV)"
    elif name == "Zenith":
        label = "θ<sub>reco</sub> (deg)"
    else:
        label = "NO LABEL SET"
    return label


def prediction_1d_histogram_x_axis(name):
    if name == "Energy":
        label = "log<sub>10</sub> E (E / GeV)"
    elif name == "Zenith":
        label = "θ (deg)"
    else:
        label = "NO LABEL SET"
    return label

def prediction_2d_histogram_y_axis(name):
    if name == "Energy":
        label = "log<sub>10</sub> E<sub>truth</sub> (E / GeV)"
    elif name == "Zenith":
        label = "θ<sub>true</sub> (deg)"
    else:
        label = "NO LABEL SET"
    return label
