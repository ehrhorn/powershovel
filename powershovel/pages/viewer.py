import pickle
import sqlite3
from logging import error
from pathlib import Path

import numpy as np
import pandas as pd
import powershovel.modules.helper_functions as hf
import streamlit as st
from powershovel.modules.helper_functions import (
    convert_cartesian_to_spherical, convert_spherical_to_cartesian)
from powershovel.plots.viewer import plotly_event


@st.cache
def get_pool(db_uri):
    query = f"""
        select * from truth limit -1
    """
    with sqlite3.connect(db_uri, uri=True) as con:
        pool = pd.read_sql(query, con)
    if np.amax(pool["azimuth"]) < 7:
        pool["azimuth"] = pool["azimuth"].apply(np.rad2deg)
    if np.amax(pool["zenith"]) < 4:
        pool["zenith"] = pool["zenith"].apply(np.rad2deg)
    return pool


@st.cache
def get_bins_in(bin_in, pool):
    bins, bin_edges = create_bins(bin_in, pool)
    return bins, bin_edges
    return bins, bin_edges


@st.cache
def create_bins(bin_in, truth):
    if bin_in == "Energy":
        minimum = np.around(np.amin(truth["energy_log10"]), 1)
        maximum = np.around(np.amax(truth["energy_log10"]), 1)
        range_span = maximum - minimum
        bins = int(range_span * 6)
        _, bin_edges = np.histogram(truth["energy_log10"], bins=bins)
    elif bin_in == "Zenith":
        minimum = 0
        maximum = np.pi
        bins = 6
        _, bin_edges = np.histogram(truth["zenith"], bins=bins)
    elif bin_in == "Azimuth":
        minimum = 0
        maximum = 2 * np.pi
        bins = 12
        _, bin_edges = np.histogram(truth["azimuth"], bins=bins)
    else:
        bins = 12
        _, bin_edges = np.histogram(truth[bin_in], bins=bins)
    return bins, bin_edges


@st.cache
def filter_events(bin_in, bin_no, bin_edges, truth):
    low = bin_edges[bin_no]
    high = bin_edges[bin_no + 1]
    if bin_in == "Energy":
        valid_events = truth[
            (truth["energy_log10"] >= low) & (truth["energy_log10"] <= high)
        ]
    elif bin_in == "Zenith":
        valid_events = truth[(truth["zenith"] >= low) & (truth["zenith"] <= high)]
    elif bin_in == "Azimuth":
        valid_events = truth[(truth["azimuth"] >= low) & (truth["azimuth"] <= high)]
    else:
        valid_events = truth[(truth[bin_in] >= low) & (truth[bin_in] <= high)]
    return valid_events


@st.cache
def get_reconstructions(dataset):
    errors_db = dataset / "errors" / "errors.db"
    errors_uri = f"file:{errors_db}?mode=ro"
    with sqlite3.connect(errors_uri, uri=True) as con:
        cur = con.cursor()
        cur.execute("SELECT name FROM sqlite_master WHERE type='table';")
        tables = cur.fetchall()
    return tables


def get_reconstruction(dataset, run, event_no):
    predictions_db = dataset / "predictions" / "predictions.db"
    predictions_uri = f"file:{predictions_db}?mode=ro"
    query = f"select * from '{run}' where event_no = {event_no}"
    with sqlite3.connect(predictions_uri, uri=True) as con:
        reconstruction = pd.read_sql(query, con)
    return reconstruction


@st.cache
def shuffle_dataset(valid_events):
    valid_events = valid_events.sample(frac=1).reset_index(drop=True)
    return valid_events


@st.cache
def get_errors(dataset, run):
    errors_db = dataset / "errors" / "errors.db"
    errors_uri = f"file:{errors_db}?mode=ro"
    query = f"select * from '{run}'"
    with sqlite3.connect(errors_uri, uri=True) as con:
        cur = con.execute(query)
        names = [
            description[0]
            for description in cur.description
            if description[0] != "event_no"
        ]
        pool = pd.read_sql(query, con)
    return names, pool


@st.cache
def merge_frames(valid_events, error_valid_events):
    error_event_nos = error_valid_events["event_no"].values.flatten()
    merged = valid_events[valid_events["event_no"].isin(error_event_nos)]
    return merged


def viewer():
    datasets_root_path = Path().home().joinpath("work").joinpath("datasets")
    valid_names = [
        "dev_genie_numu_cc_train_retro_000",
        "dev_genie_numu_cc_train_retro_001",
        "dev_genie_numu_cc_train_retro_004",
        "dev_upgrade_train_step4_000",
        "dev_upgrade_train_step4_004",
        "dev_upgrade_train_step4_005",
    ]
    datasets = [
        folder
        for folder in datasets_root_path.iterdir()
        if folder.is_dir() and folder.name in valid_names
    ]
    datasets_metas = [folder.joinpath("meta") for folder in datasets]
    valid_datasets = [
        folder.parent
        for folder in datasets_metas
        if folder.joinpath("powershovel_events.db").is_file()
    ]
    dataset = st.sidebar.selectbox(
        "Dataset", valid_datasets, 1, format_func=lambda x: x.name
    )
    runs = get_reconstructions(dataset)
    runs = [run[0] for run in runs if run[0] != "truth"]
    run = st.sidebar.selectbox(
        "Reconstructions", runs, 0, format_func=lambda x: x.split(".")[-1]
    )
    error_metrics, error_pool = get_errors(dataset, run)
    meta_path = dataset / "meta"
    geom = [f for f in meta_path.iterdir() if f.suffix == ".db" and ".i3" in f.name][0]
    geom_uri = f"file:{geom}?mode=ro"
    query = "select * from geometry"
    with sqlite3.connect(geom_uri, uri=True) as con:
        geom = pd.read_sql(query, con)
    db = dataset / "meta" / "powershovel_events.db"
    db_uri = f"file:{db}?mode=ro"
    pool = get_pool(db_uri)
    cleaning = st.sidebar.selectbox(
        "Cleaning", ["SplitInIcePulses", "SRTInIcePulses"], 0
    )
    bin_in = st.sidebar.selectbox("Bin in", ["Energy", "Zenith", "Azimuth"], 0)
    bins, bin_edges = get_bins_in(bin_in, pool)
    bin_no = st.sidebar.slider("Bin", 0, bins - 1, 0)
    valid_events = filter_events(bin_in, bin_no, bin_edges, pool)
    error_bin_type = st.sidebar.selectbox("Error metric", error_metrics, 0)
    error_bins, error_bin_edges = get_bins_in(error_bin_type, error_pool)
    error_bin_no = st.sidebar.slider("Bin", 0, error_bins - 1, 0)
    error_valid_events = filter_events(
        error_bin_type, error_bin_no, error_bin_edges, error_pool
    )
    merged_valid_events = merge_frames(valid_events, error_valid_events)
    if merged_valid_events.empty:
        st.write("No events fulfil criteria! Try other bins.")
    else:
        if st.sidebar.button("New event"):
            merged_valid_events = shuffle_dataset(merged_valid_events)
            event_no = merged_valid_events["event_no"].iloc[0]
        else:
            event_no = merged_valid_events["event_no"].iloc[0]
        truth = pool[pool["event_no"] == event_no]
        errors = error_pool[error_pool["event_no"] == event_no]
        query = f"select * from features where event_no = {event_no} and {cleaning} = 1"
        with sqlite3.connect(db_uri, uri=True) as con:
            pulses = pd.read_sql(query, con)
        reconstruction = get_reconstruction(dataset, run, event_no)
        if bin_no == bins - 1:
            print_bin_edges = (
                f"[{round(bin_edges[bin_no], 2)}, {round(bin_edges[bin_no + 1], 2)}]"
            )
        else:
            print_bin_edges = (
                f"[{round(bin_edges[bin_no], 2)}, {round(bin_edges[bin_no + 1], 2)})"
            )
        if error_bin_no == error_bins - 1:
            error_print_bin_edges = f"[{round(error_bin_edges[error_bin_no], 2)}, {round(error_bin_edges[error_bin_no + 1], 2)}]"
        else:
            error_print_bin_edges = f"[{round(error_bin_edges[error_bin_no], 2)}, {round(error_bin_edges[error_bin_no + 1], 2)})"
        raw_length = len(pulses[pulses["SplitInIcePulses"] == 1])
        clean_length = len(pulses[pulses["SRTInIcePulses"] == 1])
        st.markdown(
            f"Event {truth['event_no'].values[0]}, {bin_in} bin {print_bin_edges}, "
            f"{error_bin_type} bin {error_print_bin_edges}, "
            f"length raw: {raw_length}, length clean: {clean_length}"
        )
        reconstruction_metrics = [
            column for column in reconstruction.columns if column != "event_no"
        ]
        reconstruction.drop("event_no", axis=1, inplace=True)
        if all(
            elem in reconstruction_metrics
            for elem in ["direction_x", "direction_y", "direction_z"]
        ):
            plot_reconstruction = reconstruction
        elif reconstruction_metrics == ["zenith"]:
            plot_reconstruction = reconstruction
        elif all(elem in reconstruction_metrics for elem in ["azimuth", "zenith"]):
            direction_vectors = convert_spherical_to_cartesian(
                reconstruction["zenith"].values, reconstruction["azimuth"].values
            )
            reconstruction["direction_x"] = direction_vectors[0, 0]
            reconstruction["direction_y"] = direction_vectors[0, 1]
            reconstruction["direction_z"] = direction_vectors[0, 2]
            plot_reconstruction = reconstruction
        else:
            plot_reconstruction = None
        fig = plotly_event(pulses, geom, truth, plot_reconstruction)
        st.plotly_chart(fig, use_container_width=True)
        st.markdown("Truth")
        st.write(truth[["energy_log10", "azimuth", "zenith"]])
        if reconstruction is not None:
            try:
                azimuth, zenith = convert_cartesian_to_spherical(
                    reconstruction[["direction_x", "direction_y", "direction_z"]].values
                )
                reconstruction["azimuth"] = azimuth
                reconstruction["zenith"] = zenith
            except Exception:
                pass
            st.markdown("Reconstruction")
            st.write(reconstruction)
            st.markdown("Errors")
            st.write(errors)
        st.markdown("Pulses")
        st.write(
            pulses[["dom_x", "dom_y", "dom_z", "pmt_x", "pmt_y", "pmt_z", "pmt_type"]]
        )
