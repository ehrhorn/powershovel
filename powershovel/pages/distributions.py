from pathlib import Path

import powershovel.modules.helper_functions as hf
import streamlit as st
from powershovel.modules.helper_functions import get_dataset_paths
from powershovel.plots.distributions import bar_chart, histogram_1d


def distributions():
    datasets_root_path = Path().home().joinpath("work").joinpath("datasets")
    valid_names = [
        # "dev_genie_numu_cc_train_retro_000",
        # "dev_genie_numu_cc_train_retro_001",
        "dev_genie_numu_cc_train_retro_003",
        "dev_genie_numu_cc_train_retro_004",
        "dev_genie_numu_cc_train_retro_005",
        "dev_upgrade_train_step4_006",
        # "dev_upgrade_train_step4_000",
        # "dev_upgrade_train_step4_004",
        # "dev_upgrade_train_step4_005",
    ]
    datasets = [
        folder
        for folder in datasets_root_path.iterdir()
        if folder.is_dir() and folder.name in valid_names
    ]
    datasets_metas = [folder.joinpath("meta") for folder in datasets]
    valid_datasets = [
        folder.parent
        for folder in datasets_metas
        if folder.joinpath("distributions.pkl").is_file()
    ]
    dataset = st.sidebar.selectbox(
        "Dataset", valid_datasets, 0, format_func=lambda x: x.name
    )
    histograms = hf.open_pickle_file(
        dataset.joinpath("meta").joinpath("distributions.pkl")
    )
    transforms = list(histograms.keys())
    transform = st.sidebar.selectbox("Transform", transforms, 0)
    histograms = histograms[transform]

    tables = list(histograms.keys())
    table = st.sidebar.selectbox("Table", tables, 0)
    histograms = histograms[table]

    columns = list(histograms.keys())
    column = st.sidebar.selectbox("Column", columns, 0)
    histograms = histograms[column]

    chart_type = histograms[list(histograms.keys())[0]]["type"]

    density = st.sidebar.radio("Density", [True, False], index=1)

    if chart_type == "histogram":
        fig = histogram_1d(histograms, column, density=density)
        st.plotly_chart(fig, use_container_width=True)
    elif chart_type == "bar":
        fig = bar_chart(histograms, column, density=density)
        st.plotly_chart(fig, use_container_width=True)
