import pickle
from pathlib import Path

import numpy as np
import pandas as pd
import powershovel.modules.helper_functions as hf
import powershovel.plots.runs as run_plots
import streamlit as st


def runs():
    done_runs_path = Path().home().joinpath("work").joinpath("runs").joinpath("done")
    reconstructions_path = (
        Path().home().joinpath("work").joinpath("runs").joinpath("reconstructions")
    )
    types = ["Energy", "Direction", "Vertex", "Time", "Angle", "Zenith"]
    selected_type = st.sidebar.selectbox("Type", types, 5)
    raw_valid_runs = [
        folder
        for folder in done_runs_path.iterdir()
        if folder.joinpath(folder.name + "_histograms.pkl").is_file()
        and selected_type.lower() in folder.name
    ]
    filters = []
    filters.extend(["model: " + str(run).split(".")[2] for run in raw_valid_runs])
    filters.extend(["cleaning: " + str(run).split(".")[3] for run in raw_valid_runs])
    filters.extend(["optimizer: " + str(run).split(".")[4] for run in raw_valid_runs])
    filters.extend(["loss: " + str(run).split(".")[5] for run in raw_valid_runs])
    filters.extend(["weight: " + str(run).split(".")[6] for run in raw_valid_runs])
    filters.extend(["patience: " + str(run).split(".")[7] for run in raw_valid_runs])
    filters.extend(
        ["train_batch_size: " + str(run).split(".")[8] for run in raw_valid_runs]
    )
    filters.extend(["max_length: " + str(run).split(".")[9] for run in raw_valid_runs])
    filters = list(dict.fromkeys(filters))
    selected_filters = st.sidebar.multiselect("Run filters", filters)
    if selected_filters is not None:
        selected_filters = [
            selected_filter.split(": ")[-1] for selected_filter in selected_filters
        ]
        filtered_valid_runs = [
            folder
            for folder in raw_valid_runs
            if all(
                [selected_filter in str(folder) for selected_filter in selected_filters]
            )
        ]
        valid_runs = filtered_valid_runs
    else:
        valid_runs = raw_valid_runs
    selected_run = st.sidebar.selectbox(
        "Run", valid_runs, 0, format_func=lambda x: x.name.split(".")[-1]
    )
    selected_run_histograms = hf.open_pickle_file(
        selected_run.joinpath(selected_run.name + "_histograms.pkl")
    )
    selected_run_perm_imp = hf.open_pickle_file(selected_run.joinpath("perm_imp.pkl"))
    selected_prediction_set = st.sidebar.selectbox(
        "Run prediction set", list(selected_run_histograms.keys())
    )
    selected_run_histograms = selected_run_histograms[selected_prediction_set]

    filters = []
    filters.extend(["model: " + str(run).split(".")[2] for run in raw_valid_runs])
    filters.extend(["cleaning: " + str(run).split(".")[3] for run in raw_valid_runs])
    filters.extend(["optimizer: " + str(run).split(".")[4] for run in raw_valid_runs])
    filters.extend(["loss: " + str(run).split(".")[5] for run in raw_valid_runs])
    filters.extend(["weight: " + str(run).split(".")[6] for run in raw_valid_runs])
    filters.extend(["patience: " + str(run).split(".")[7] for run in raw_valid_runs])
    filters.extend(
        ["train_batch_size: " + str(run).split(".")[8] for run in raw_valid_runs]
    )
    filters.extend(["max_length: " + str(run).split(".")[9] for run in raw_valid_runs])
    filters = list(dict.fromkeys(filters))
    selected_filters = st.sidebar.multiselect("Comparison filters", filters)
    if selected_filters is not None:
        selected_filters = [
            selected_filter.split(": ")[-1] for selected_filter in selected_filters
        ]
        filtered_valid_runs = [
            folder
            for folder in raw_valid_runs
            if all(
                [selected_filter in str(folder) for selected_filter in selected_filters]
            )
        ]
        valid_runs = filtered_valid_runs
    else:
        valid_runs = raw_valid_runs
    valid_reconstructions = [
        folder
        for folder in reconstructions_path.iterdir()
        if folder.joinpath(folder.name + "_histograms.pkl").is_file()
        and selected_type.lower() in folder.name
    ]
    valid_runs = valid_runs + valid_reconstructions
    selected_comparison = st.sidebar.selectbox(
        "Comparison", valid_runs, 1, format_func=lambda x: x.name.split(".")[-1]
    )
    selected_comparison_histograms = hf.open_pickle_file(
        selected_comparison.joinpath(selected_comparison.name + "_histograms.pkl")
    )
    selected_comparison_perm_imp = hf.open_pickle_file(
        selected_comparison.joinpath("perm_imp.pkl")
    )
    selected_prediction_set = st.sidebar.selectbox(
        "Comparison prediction set", list(selected_comparison_histograms.keys())
    )
    selected_comparison_histograms = selected_comparison_histograms[
        selected_prediction_set
    ]

    metrics = list(selected_run_histograms.keys())
    selected_metric = st.sidebar.selectbox("Metric", metrics)

    selected_run_histograms = selected_run_histograms[selected_metric]
    selected_run_histograms["permutation_importance_bar_chart"] = selected_run_perm_imp
    selected_comparison_histograms = selected_comparison_histograms[selected_metric]
    selected_comparison_histograms[
        "permutation_importance_bar_chart"
    ] = selected_comparison_perm_imp

    selected_bin = st.sidebar.slider("Bin", min_value=1, max_value=18)

    selected_run_name = selected_run.name.split(".")[-1]
    selected_comparison_name = selected_comparison.name.split(".")[-1]

    table = pd.DataFrame(
        np.array(
            [
                [
                    selected_run.name.split(".")[2],
                    selected_comparison.name.split(".")[2],
                ],
                [
                    selected_run.name.split(".")[3],
                    selected_comparison.name.split(".")[3],
                ],
                [
                    selected_run.name.split(".")[4],
                    selected_comparison.name.split(".")[4],
                ],
                [
                    selected_run.name.split(".")[5],
                    selected_comparison.name.split(".")[5],
                ],
                [
                    selected_run.name.split(".")[6],
                    selected_comparison.name.split(".")[6],
                ],
                [
                    selected_run.name.split(".")[7],
                    selected_comparison.name.split(".")[7],
                ],
                [
                    selected_run.name.split(".")[8],
                    selected_comparison.name.split(".")[8],
                ],
                [
                    selected_run.name.split(".")[9],
                    selected_comparison.name.split(".")[9],
                ],
            ]
        ),
        columns=[selected_run_name, selected_comparison_name],
        index=[
            "model",
            "cleaning",
            "optimizer",
            "loss",
            "weights",
            "patience",
            "train_batch_size",
            "max_length",
        ],
    )

    st.table(table)

    for key in selected_run_histograms:
        print(key)
        histograms_dict = {
            "Run: " + selected_run_name: selected_run_histograms[key],
            "Comparison: "
            + selected_comparison_name: selected_comparison_histograms[key],
        }
        if key != "error_2d_histogram" and key != "prediction_2d_histogram":
            plot_function = getattr(run_plots, key)
            fig = plot_function(histograms_dict, selected_type, selected_bin - 1)
            st.plotly_chart(fig, use_container_width=True)
